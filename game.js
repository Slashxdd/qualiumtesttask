var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");
var rectangles = new Array();
var score = document.getElementById("score");
var isStart = 0;
var startBtn = document.getElementById("startBtn");
var stopBtn = document.getElementById("stopBtn");
stopBtn.disabled = true;

// initialize rectangle with random speed and color
const Rect = function(x, y, width, height) {
    this.color = "rgb(" + Math.floor(Math.random() * 256) + "," + Math.floor(Math.random() * 256) + "," + Math.floor(Math.random() * 256) + ")";
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
    this.speed = Math.random() * 3;
  };
  Rect.prototype = {
    updatePosition:function() {
      this.y += this.speed;
  }
};

function loop() {
  context.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);
  requestId = window.requestAnimationFrame(loop);

// loop for render rectangles
  for(let index = 0; index < rectangles.length; index++) {
    let rectangle = rectangles[index];
    context.fillStyle = rectangle.color;
    context.beginPath();
    context.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    context.fill();
    rectangle.updatePosition();
  }
}

function start() {
  startBtn.disabled = true;
  stopBtn.disabled = false;

    loop();
    setInterval(() => {
     console.log("done");
     //console.log(rectangles.length);
     rectangles.push(new Rect(Math.random() * 620, 0, 20, 20));
   }, Math.random()*1500);
}

function stop() {
  startBtn.disabled = false;
  stopBtn.disabled = true;

  for (var i = 1; i < 99999; i++)
   window.clearInterval(i);
  window.cancelAnimationFrame(requestId);
  rectangles = [];
  context.clearRect(0, 0, canvas.width, canvas.height);
	score.innerHTML = 0;
}

var isCursorInSquares = function(x, y, rectangle) {
  return  x > rectangle.x && x < rectangle.x + rectangle.width + 10 && y > rectangle.y && y < rectangle.y
  + rectangle.height + 20;
}

canvas.onclick = function(e) {
  var x = e.pageX;
  var y = e.pageY;

  for(var i = rectangles.length - 1; i >= 0; --i){
    if(isCursorInSquares(x, y, rectangles[i])) { 
      delete rectangles.splice(i, 1);
      score.innerHTML = Number(score.innerHTML)+1;
    } 
  }
}
